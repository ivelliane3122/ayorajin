package com.example.ayorajin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class HomeActivity extends AppCompatActivity {


    private RecyclerView todoListView, doneTodoList;

    EditText formNewTugas;
    todoListAdapter adapter;
    private Button addButton;
    TextView userName;
    Button logout;
    GoogleSignInClient gClient;
    GoogleSignInOptions gOptions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference =  firebaseDatabase.getReference("tugas");
        Query queryReference = databaseReference.orderByChild("statusId").equalTo("s1");

        todoListView = findViewById(R.id.todoList);


        formNewTugas = findViewById(R.id.formNewTugas);

        todoListView.setLayoutManager(new LinearLayoutManager(this));


        addButton = findViewById(R.id.addButton);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newTugas = formNewTugas.getText().toString().trim();

                if(newTugas.isEmpty()){
                    Toast.makeText(HomeActivity.this, "Please Input Todo List", Toast.LENGTH_SHORT).show();

                } else {
                    todoList newTodoList = new todoList(newTugas, "s1");

                    DatabaseReference newChildRef = databaseReference.push();
                    newChildRef.setValue(newTodoList);

                    formNewTugas.setText("");
                }
            }
        });

        FirebaseRecyclerOptions<todoList> options = new FirebaseRecyclerOptions.Builder<todoList>().setQuery(databaseReference, todoList.class).build();

        adapter = new todoListAdapter(options);

        todoListView.setAdapter(adapter);





//        databaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                if (dataSnapshot.exists()) {
//                    // Retrieve the data from the dataSnapshot
//                    String data = dataSnapshot.getValue(String.class);
//
//                    // Update the TextView with the retrieved data
//                    dataTextView.setText(data);
//                } else {
//                    dataTextView.setText("No data found.");
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//                // Handle any errors here
//                Toast.makeText(HomeActivity.this, "Fail to get data.", Toast.LENGTH_SHORT).show();
//            }
//        });


        // User ID
        mFirebaseUser.getUid();
        // Email-ID
        mFirebaseUser.getEmail();

        logout = findViewById(R.id.logout);
        userName = findViewById(R.id.userName);

        userName.setText(mFirebaseUser.getEmail());

        gOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        gClient = GoogleSignIn.getClient(this, gOptions);

        GoogleSignInAccount gAccount = GoogleSignIn.getLastSignedInAccount(this);
        if (gAccount != null){
            String gName = gAccount.getEmail();
            userName.setText(gName);
        }
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        finish();
                        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                    }
                });
            }
        });


    }
    @Override protected void onStart()
    {
        super.onStart();
        adapter.startListening();
    }

    // Function to tell the app to stop getting
    // data from database on stopping of the activity
    @Override protected void onStop()
    {
        super.onStop();
        adapter.stopListening();
    }

}