package com.example.ayorajin;

public class todoList {
    private String name, statusId;


    public todoList(){

    }
    public todoList(String name, String statusId){
        this.name = name;
        this.statusId = statusId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatusId(){
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }
}
