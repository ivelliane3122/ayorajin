package com.example.ayorajin;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;

public class todoListAdapter extends FirebaseRecyclerAdapter<todoList, todoListAdapter.todoListViewholder> {
    public todoListAdapter(
            @NonNull FirebaseRecyclerOptions<todoList> options
    ){
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull todoListViewholder holder, int position, @NonNull todoList model) {
        holder.bind(model);
    }

    @NonNull
    @Override
    public todoListViewholder
    onCreateViewHolder(@NonNull ViewGroup parent,
                       int viewType)
    {
        View view
                = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.todo_list, parent, false);
        return new todoListAdapter.todoListViewholder(view);
    }

    class todoListViewholder
            extends RecyclerView.ViewHolder {
        TextView tugas;
        Button changeStatusButton;
        public todoListViewholder(@NonNull View itemView)
        {
            super(itemView);

            tugas = itemView.findViewById(R.id.tugas);
            changeStatusButton = itemView.findViewById(R.id.changeStatusButton);
        }
        public void bind(todoList model){
            tugas.setText(model.getName());

            if (!model.getStatusId().equals("s1")){
                tugas.setPaintFlags(tugas.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                tugas.setPaintFlags(tugas.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            }

            if (model.getStatusId().equals("s1")) {
                changeStatusButton.setVisibility(View.VISIBLE); // Show the button
            } else {
                changeStatusButton.setVisibility(View.GONE); // Hide the button
            }
            changeStatusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Handle the status change button click
                    // You can update the 'statusId' in the Firebase Realtime Database here
                    // For example, toggle between 's1' and another status
                    if (model.getStatusId().equals("s1")) {
                        model.setStatusId("s2");
                    } else {
                        model.setStatusId("s1");
                    }

                    // Update the 'statusId' in the Firebase Realtime Database
                    DatabaseReference itemRef = getRef(getAdapterPosition());
                    itemRef.child("statusId").setValue(model.getStatusId());
                }
            });
        }
    }
}
